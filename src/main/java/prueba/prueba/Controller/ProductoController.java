package prueba.prueba.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import prueba.prueba.model.Producto;
import prueba.prueba.service.ProductoService;

@CrossOrigin
@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private ProductoService productoService;


	@GetMapping
	public List<Producto> obtenerTodosLosProductos() {
		return productoService.obtenerTodos();
	}

	@GetMapping("/{id}")
	public Producto obtenerProductoPorId(@PathVariable Long id) {
		return productoService.obtenerPorId(id);
	}

	@GetMapping("/prueba")
	public String prueba() {
		return "Hola Mundo";
	}

	@PostMapping
	public Producto guardarProducto(@RequestBody Producto producto) {
		return productoService.guardarProducto(producto);
	}

	@PutMapping("/{id}")
	public Producto actualizarProducto(@PathVariable Long id, @RequestBody Producto producto) {
		Producto productoExistente = productoService.obtenerPorId(id);
		if (productoExistente != null) {
			productoExistente.setNombre(producto.getNombre());
			productoExistente.setPrecio(producto.getPrecio());
			return productoService.guardarProducto(productoExistente);
		} else {
			return null; //
		}
	}

	@PostMapping("/delete")
	public void eliminarProducto(@RequestBody Producto prd) {
		productoService.eliminarProducto(prd);
	}
}
