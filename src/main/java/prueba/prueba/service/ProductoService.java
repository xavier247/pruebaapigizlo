package prueba.prueba.service;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import prueba.prueba.model.Producto;
import prueba.prueba.repository.ProductoRepository;

@Service
public class ProductoService {

	 @Autowired
	    private ProductoRepository productoRepository;

	    public List<Producto> obtenerTodos() {
	        return productoRepository.findAll();
	    }

	    public Producto obtenerPorId(Long id) {
	    	
	        return new Producto();
	    }

	    public Producto guardarProducto(Producto producto) {
	        return productoRepository.save(producto);
	    }

	    public void eliminarProducto(Producto prd) {
	        productoRepository.delete(prd);
	    }
}
