package prueba.prueba.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import prueba.prueba.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

	
	
	    	
}
