# Usa la imagen base de OpenJDK para Java 1.8
FROM openjdk:1.8-jre-slim

# Copia el JAR (o WAR) de tu aplicación Spring Boot al contenedor y nómbralo como 'app.jar'
COPY target/nombre-de-tu-aplicacion.jar app.jar

# Exponer el puerto que utiliza tu aplicación Spring Boot (por ejemplo, el puerto 8080)
EXPOSE 8080

# Comando para ejecutar tu aplicación Spring Boot cuando se inicie el contenedor
CMD ["java", "-jar", "app.jar"]
